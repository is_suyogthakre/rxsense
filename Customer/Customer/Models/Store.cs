﻿using System;
using System.Collections.Generic;

namespace Customer.Models
{
    public partial class Store
    {
        public Store()
        {
            Customer = new HashSet<Customer>();
        }

        public int StoreId { get; set; }
        public int? CountryId { get; set; }
        public string StoreName { get; set; }

        public Country Country { get; set; }
        public ICollection<Customer> Customer { get; set; }
    }
}
