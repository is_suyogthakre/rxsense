﻿using System;
using System.Collections.Generic;

namespace Customer.Models
{
    public partial class Country
    {
        public Country()
        {
            Store = new HashSet<Store>();
        }

        public int CountryId { get; set; }
        public string CountryName { get; set; }

        public ICollection<Store> Store { get; set; }
    }
}
