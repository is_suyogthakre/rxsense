﻿using System;
using System.Collections.Generic;

namespace Customer.Models
{
    public partial class Customer
    {
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public int? StoreId { get; set; }
        public DateTime? CreatedDate { get; set; }

        public Store Store { get; set; }
    }
}
