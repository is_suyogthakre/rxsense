﻿using Customer.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Customer.Repository
{
    public class PostRepository : IPostRepository
    {
        CustomerContext db;
        public PostRepository(CustomerContext _db)
        {
            db = _db;
        }

        public async Task<List<Store>> GetStores(int CountryId)
        {
            if (db != null)
            {
                return await db.Store.Where(x => x.CountryId == CountryId).ToListAsync();
            }

            return null;
        }

        public async Task<int> AddCustomer(Models.Customer customer)
        {
            if (db != null)
            {
                customer.CreatedDate = DateTime.Now;
                await db.Customer.AddAsync(customer);
                await db.SaveChangesAsync();

                return customer.CustomerId;
            }

            return 0;
        }

        public async Task<List<Models.Customer>> GetStoreCustomers(int storeId)
        {
            if (db != null)
            {
                return await db.Customer.Where(x => x.StoreId == storeId).ToListAsync();
            }

            return null;
        }

        public async Task<int> DeleteCustomer(int? customerId)
        {
            int result = 0;

            if (db != null)
            {
                var post = await db.Customer.FirstOrDefaultAsync(x => x.CustomerId == customerId);

                if (post != null)
                {
                    db.Customer.Remove(post);

                    //Commit the transaction
                    result = await db.SaveChangesAsync();
                }
                return result;
            }

            return result;

        }

        public async Task UpdateCustomer(Models.Customer customer)
        {
            if (db != null)
            {
                var result = await db.Customer.Where(x => x.CustomerId == customer.CustomerId).FirstOrDefaultAsync();
                db.Entry(result).State = EntityState.Detached;
                db.Customer.Update(customer);
                //Commit the transaction
                await db.SaveChangesAsync();
            }
        }

        public async Task<Models.Customer> GetCustomer(int customerId)
        {
            var result = await db.Customer.Where(x => x.CustomerId == customerId).FirstOrDefaultAsync();
            return result;

        }
    }
}
