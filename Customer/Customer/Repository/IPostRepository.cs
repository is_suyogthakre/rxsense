﻿using Customer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Customer.Repository
{
    public interface IPostRepository
    {
        Task<List<Store>> GetStores(int CountryId);
        Task<int> AddCustomer(Models.Customer customer);
        Task<List<Models.Customer>> GetStoreCustomers(int storeId);
        Task<int> DeleteCustomer(int? customerId);
        Task UpdateCustomer(Models.Customer customer);
        Task<Models.Customer> GetCustomer(int customerId);
    }
}
