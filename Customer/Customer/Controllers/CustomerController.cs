﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Customer.Repository;
using Customer.Validators;
using Microsoft.AspNetCore.Mvc;

namespace Customer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : Controller
    {
        IPostRepository postRepository;
        public CustomerController(IPostRepository _postRepository)
        {
            postRepository = _postRepository;
        }

        [HttpGet]
        [Route("GetStore")]
        public async Task<IActionResult> GetStores(int CountryId)
        {
            try
            {
                var categories = await postRepository.GetStores(CountryId);
                if (categories == null)
                {
                    return NotFound();
                }

                return Ok(categories);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }

        }

        [HttpPost]
        [Route("AddCustomer")]
        public async Task<IActionResult> AddCustomer([FromBody]Models.Customer model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var postId = await postRepository.AddCustomer(model);
                    if (postId > 0)
                    {
                        return Ok(postId);
                    }
                    else
                    {
                        return NotFound();
                    }
                }
                catch (Exception ex)
                {

                    return BadRequest();
                }

            }

            return BadRequest();
        }

        [HttpGet]
        [Route("GetStoreCustomers")]
        public async Task<IActionResult> GetStoreCustomers(int storeId)
        {
            try
            {
                var storeCustomers = await postRepository.GetStoreCustomers(storeId);
                if (storeCustomers == null)
                {
                    return NotFound();
                }

                return Ok(storeCustomers);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }
        [HttpDelete]
        [Route("DeleteCustomer")]
        public async Task<IActionResult> DeleteCustomer(int? customerId)
        {
            try
            {
                int result = 0;

                if (customerId == null)
                {
                    return BadRequest();
                }

                try
                {
                    result = await postRepository.DeleteCustomer(customerId);
                    if (result == 0)
                    {
                        return NotFound();
                    }
                    return Ok();
                }
                catch (Exception)
                {

                    return BadRequest();
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        [HttpPut]
        [Route("UpdateCustomer")]
        public async Task<IActionResult> UpdateCustomer([FromBody]Models.Customer customer)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var result = await postRepository.GetCustomer(customer.CustomerId);
                    if (result == null)
                    {
                        return NotFound();
                    }
                    await postRepository.UpdateCustomer(customer);

                    return Ok();
                }
                catch (Exception ex)
                {
                    if (ex.GetType().FullName ==
                             "Microsoft.EntityFrameworkCore.DbUpdateConcurrencyException")
                    {
                        return NotFound();
                    }

                    return BadRequest();
                }
            }

            return BadRequest();
        }

    }
}