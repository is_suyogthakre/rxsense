﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Customer.Validators
{
    public class CustomerValidator :AbstractValidator<Models.Customer>
    {
        public CustomerValidator()
        {
            RuleFor(m => m.CustomerName).NotEmpty();
        }
    }
}
