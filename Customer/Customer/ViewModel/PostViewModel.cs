﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Customer.ViewModel
{
    public class PostViewModel
    {
        public int StoreId { get; set;}
        public int CountryId { get; set; }
        public int CustomerId { get; set; }
        public string StoreName { get; set; }
        public string CountryName { get; set; }
        public string CustomerName { get; set; }
    }
}
