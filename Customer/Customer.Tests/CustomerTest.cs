﻿using Customer.Controllers;
using Customer.Models;
using Customer.Repository;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Customer.Tests
{
    public class CustomerTest
    {
        private readonly Mock<IPostRepository> mockRepo;
        private readonly CustomerController controller;
        public CustomerTest()
        {
            mockRepo = new Mock<IPostRepository>();
            controller = new CustomerController(mockRepo.Object);
        }

        [Theory]
        [InlineData(1)]
        public async Task Get_Returns_All_Store_By_CountyId(int countryId)
        {
            // Arrange
            this.mockRepo.Setup(r => r.GetStores(It.IsAny<int>())).ReturnsAsync(new List<Store> { new Store { StoreName = "Dmart", CountryId = 1, StoreId = 1 } });

            // Act
            var result = await controller.GetStores(countryId);

            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result);
            Assert.IsType<List<Store>>(okResult.Value);
            mockRepo.Verify(repo => repo.GetStores(countryId), Times.AtMostOnce);
        }
    }
}
